// console.log("Kangkong")

/*
Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
	Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
Create a variable address with a value of an array containing details of an address.
	Destructure the array and print out a message with the full address using Template Literals.
Create a variable animal with a value of an object data type with different animal details as its properties.
	name
	species
	weight
	measurement

	Destructure the object and print out a message with the details of the animal using Template Literals.
Create an array of numbers.
	Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
	Create/instantiate a new object from the class Dog and console log the object.

*/

// Get Cube of 2
	let getCube = Math.pow(2,3);
	console.log(`The cube of 2 is ${getCube}`);

// Address - Array
	const address=[258, "Washington Ave", "NW", "California", 90011]
	const [streetNum, avenue, city, state, zip]=address;
	console.log(`I live at ${streetNum} ${avenue} ${city}, ${state} ${zip}`)

// Animal - Object
	const animal = {
		name: "Lolong",
		Species: "saltwater crocodile",
		weight: "1075 kgs",
		measurement: "20 ft 3 in"
	}
	const {name, Species, weight, measurement}=animal;
	console.log(`${name} was a ${Species}. He weighed at ${weight} with a measurement of ${measurement}`)

// Array of Numbers
	let arrayNum = [1,2,3,4,5]
	let arrNum = (arrayNum) => {
		console.log(`${arrayNum}`)
	}
	arrayNum.forEach(arrNum)

// Sum of all the numbers
	let reduceNum = arrayNum.reduce(function(x,y){
		return x+y
	})
		console.log(reduceNum)

// Class of a Dog
/*
Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
	Create/instantiate a new object from the class Dog and console log the object.
*/
	class Dog{
			constructor(name, age, breed){
				this.dogName = name;
				this.dogAge = age;
				this.dogBreed = breed;
			}
		}
			let dog = new Dog("Kangkong", "4", "Labrador Retriever")
			console.log(dog);

			dog.dogWeight = "25 kg";
			console.log(dog)
